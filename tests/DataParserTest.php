<?php

declare(strict_types=1);

namespace Tests;

use Girgias\NumericalAnalysis\CW1\DataParser;
use MathPHP\LinearAlgebra\Vector;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class DataParserTest extends TestCase
{
    public function testParseData(): void
    {
        $string = ' -1.03    1.12079462
 -0.92    1.07494831
 -1.00    1.10032304
 -0.96    0.84766580
 -1.02    0.88645981
 -0.83    0.81325067';
        $dataParser = new DataParser($string);

        $x = new Vector([-1.03, -0.92, -1.00, -0.96, -1.02, -0.83]);
        $y = new Vector([
            1.12079462,
            1.07494831,
            1.10032304,
            0.84766580,
            0.88645981,
            0.81325067,
        ]);

        self::assertTrue($x->asRowMatrix()->isEqual(
            $dataParser->getX()->asRowMatrix()
        ));

        self::assertTrue($y->asRowMatrix()->isEqual(
            $dataParser->getY()->asRowMatrix()
        ));
    }

    public function testParseDataEOFAfterEOL(): void
    {
        $string = ' -1.03    1.12079462
 -0.92    1.07494831
 -1.00    1.10032304
 -0.96    0.84766580
 -1.02    0.88645981
 -0.83    0.81325067
';
        $dataParser = new DataParser($string);

        $x = new Vector([-1.03, -0.92, -1.00, -0.96, -1.02, -0.83]);
        $y = new Vector([
            1.12079462,
            1.07494831,
            1.10032304,
            0.84766580,
            0.88645981,
            0.81325067,
        ]);

        self::assertTrue($x->asRowMatrix()->isEqual(
            $dataParser->getX()->asRowMatrix()
        ));

        self::assertTrue($y->asRowMatrix()->isEqual(
            $dataParser->getY()->asRowMatrix()
        ));
    }

    public function testParseDataDifferentSizeDelimiter(): void
    {
        $string = ' -2.01    0.21807718
 -1.97    0.13556845
 -1.93   -0.01414478
 -2.08   -0.05168503';
        $dataParser = new DataParser($string);

        $x = new Vector([-2.01, -1.97, -1.93, -2.08]);
        $y = new Vector([
            0.21807718,
            0.13556845,
            -0.01414478,
            -0.05168503,
        ]);

        self::assertTrue($x->asRowMatrix()->isEqual(
            $dataParser->getX()->asRowMatrix()
        ));

        self::assertTrue($y->asRowMatrix()->isEqual(
            $dataParser->getY()->asRowMatrix()
        ));
    }
}
