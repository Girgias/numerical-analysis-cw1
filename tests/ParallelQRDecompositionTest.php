<?php

declare(strict_types=1);

namespace Tests;

use Girgias\NumericalAnalysis\CW1\ParallelQRDecomposition;
use MathPHP\LinearAlgebra\Matrix;
use MathPHP\LinearAlgebra\MatrixFactory;
use MathPHP\LinearAlgebra\Vector;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class ParallelQRDecompositionTest extends TestCase
{
    public function testGetSubMatricesOddRows(): void
    {
        $X₁ = new Vector([1, 4, 7, 15, 16, 18, 22, 24, 28]);
        $X₂ = new Vector([2, 5, 8, 11, 13, 20, 21, 29, 35]);
        $A = MatrixFactory::createFromVectors([$X₁, $X₂]);

        $A₁₁ = new Vector([1, 4, 7]);
        $A₁₂ = new Vector([2, 5, 8]);
        $A₁ = MatrixFactory::createFromVectors([$A₁₁, $A₁₂]);

        $A₂₁ = new Vector([15, 16, 18]);
        $A₂₂ = new Vector([11, 13, 20]);
        $A₂ = MatrixFactory::createFromVectors([$A₂₁, $A₂₂]);

        $A₃₁ = new Vector([22, 24, 28]);
        $A₃₂ = new Vector([21, 29, 35]);
        $A₃ = MatrixFactory::createFromVectors([$A₃₁, $A₃₂]);

        $list = ParallelQRDecomposition::getSubMatrices($A, 3);

        self::assertCount(3, $list);
        self::assertTrue($A₁->isEqual($list[0]));
        self::assertTrue($A₂->isEqual($list[1]));
        self::assertTrue($A₃->isEqual($list[2]));
    }

    public function testGetSubMatricesEvenRows(): void
    {
        $A = $this->getMatrixA();
        $A₁ = $this->getMatrixA1();
        $A₂ = $this->getMatrixA2();
        $A₃ = $this->getMatrixA3();

        $list = ParallelQRDecomposition::getSubMatrices($A, 3);

        self::assertCount(3, $list);
        self::assertTrue($A₁->isEqual($list[0]));
        self::assertTrue($A₂->isEqual($list[1]));
        self::assertTrue($A₃->isEqual($list[2]));
    }

    public function testParallelQRDecomposition(): void
    {
        $A = $this->getMatrixA();

        $ParallelQR = new ParallelQRDecomposition($A, 4);

        $aCustomQR = $ParallelQR->getQ()->multiply($ParallelQR->getR());

        self::assertTrue($A->isEqual($aCustomQR));
        self::assertTrue($ParallelQR->getR()->isUpperTriangular());
    }

    public function testMergeSubMatrices(): void
    {
        $A = $this->getMatrixA();
        $A₁ = $this->getMatrixA1();
        $A₂ = $this->getMatrixA2();
        $A₃ = $this->getMatrixA3();

        $augmentedA = ParallelQRDecomposition::mergeSubMatrices($A₁, $A₂, $A₃);

        self::assertTrue($A->isEqual($augmentedA));
    }

    private function getMatrixA(): Matrix
    {
        $X₁ = new Vector([1, 4, 7, 15, 16, 18, 22, 24, 28, 33]);
        $X₂ = new Vector([2, 5, 8, 11, 13, 20, 21, 29, 35, 38]);

        return MatrixFactory::createFromVectors([$X₁, $X₂]);
    }

    private function getMatrixA1(): Matrix
    {
        $A₁₁ = new Vector([1, 4, 7]);
        $A₁₂ = new Vector([2, 5, 8]);

        return MatrixFactory::createFromVectors([$A₁₁, $A₁₂]);
    }

    private function getMatrixA2(): Matrix
    {
        $A₂₁ = new Vector([15, 16, 18]);
        $A₂₂ = new Vector([11, 13, 20]);

        return MatrixFactory::createFromVectors([$A₂₁, $A₂₂]);
    }

    private function getMatrixA3(): Matrix
    {
        $A₃₁ = new Vector([22, 24, 28, 33]);
        $A₃₂ = new Vector([21, 29, 35, 38]);

        return MatrixFactory::createFromVectors([$A₃₁, $A₃₂]);
    }
}
