<?php

declare(strict_types=1);

namespace Tests;

use Girgias\NumericalAnalysis\CW1\LeastSquareSolver;
use MathPHP\LinearAlgebra\MatrixFactory;
use MathPHP\LinearAlgebra\Vector;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class LeastSquareSolverTest extends TestCase
{
    /*
     * This A and Y vector has been checked using the online least square solver
     * https://adrianstoll.com/linear-algebra/least-squares.html
     */
    public function testBasicSolution(): void
    {
        $X₁ = new Vector([1, 4, 7, 15, 16, 18, 22, 24, 28]);
        $X₂ = new Vector([2, 5, 8, 11, 13, 20, 21, 29, 35]);
        $A = MatrixFactory::createFromVectors([$X₁, $X₂]);

        $Y = new Vector([15, 45, 48, 69, 78, 13, 16, 78, 23]);

        $LSS = new LeastSquareSolver($A, $Y);
        $vector = $LSS->getSolution()->getVector();

        self::assertSame(6.568278126312293, $vector[0]);
        self::assertSame(-4.040988508965029, $vector[1]);
    }
}
