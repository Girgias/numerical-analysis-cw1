%%
%% This is file `texstyle-notes.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% texstyle-notes.dtx  (with options: `class')
%% options.dtx  (with options: `class')
%% ../common/colours.dtx  (with options: `package')
%% ../common/glyphs.dtx  (with options: `package')
%% ../common/environments.dtx  (with options: `package')
%% document.dtx  (with options: `class')
%% headers.dtx  (with options: `class')
%% This is a generated file
%% 
%% MIT License
%% 
%% Copyright (c) 2017 Gautam Chaudhuri
%% 
%% Permission is hereby granted, free of charge, to any person obtaining a copy
%% of this software and associated documentation files (the "Software"), to deal
%% in the Software without restriction, including without limitation the rights
%% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%% copies of the Software, and to permit persons to whom the Software is
%% furnished to do so, subject to the following conditions:
%% 
%% The above copyright notice and this permission notice shall be included in all
%% copies or substantial portions of the Software.
%% 
%% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%% SOFTWARE.
%%
%%-----------------------------------------------------------
%%-----------------------------------------------------------
%% Preamble
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{texstyle-notes}
  [2019/12/26 v0.0.6 A class for typesetting notes]
\LoadClassWithOptions{article}

%%-----------------------------------------------------------
%% Options
%% |- Dependencies
\RequirePackage{kvoptions}
\SetupKeyvalOptions{
  family=TS,
  prefix=TS@
}

%% |- Option initialisation
\DeclareBoolOption{defaultFonts} % Use the in built fonts

\DeclareBoolOption{draft} % Draft mode
\DeclareComplementaryOption{final}{draft}

\newif\ifTwosided % chktex 1
\DeclareVoidOption{twoside}{\Twosidedtrue} % Twosided document

\DeclareBoolOption[true]{HyperrefMacros} % Hyperref enabled links
\DeclareStringOption[section]{envRefreshLevel} % The level at which to refresh
                                % the

%% |- Default Option
\DeclareDefaultOption{
  \PackageWarning{texstyle-notes}{Unknown Option '\CurrentOption'}
}

\ProcessKeyvalOptions*

%%-----------------------------------------------------------
%% Colours
%% |- Dependencies
\RequirePackage{color}
\RequirePackage{xcolor}

%% |- Colours
\definecolorset{HTML}{accent-}{}{%
  red,EF2D56;%
  green,7DDF64;%
  blue,5BC0EB%
}

\definecolorset{HTML}{line-}{}{%
  blue,1D2951;%
  grey,90909A;%
  black,02111B%
}

\definecolorset{HTML}{back-}{}{%
  grey,403F4C;%
  blue,C6E0FF%
}

\definecolorset{HTML}{ts-}{}{%
  0,FFFFEE;%
  1,F1F1F0;%
  2,F0F0EE;%
  3,F2E7DA;%
  4,F2BC79;%
  5,C2D2E9;%
  6,5E83BA;%
  7,8899AA;%
  8,3A4E7A;%
  9,004AA8;%
  10,114488;%
  11,003366;%
  12,2B2B2B;%
  13,091D36;%
  14,221100;%
  15,111110%
}

%%------------------------------------------------------------
%% Glyphs
  \RequirePackage{amssymb}
  \RequirePackage{amsmath}
  \RequirePackage{centernot}
  \RequirePackage{bbm}
  \RequirePackage[no-math]{fontspec}

  \renewcommand{\implies}{\>\Longrightarrow\>}
  \newcommand{\implied}{\>\Longleftarrow\>}
  \newcommand{\nImplies}{\>\centernot\Longrightarrow\>}
  \newcommand{\nImplied}{\>\centernot\Longleftarrow\>}
  \renewcommand{\iff}{\>\Longleftrightarrow\>}
  \newcommand{\nIff}{\>\centernot\Longleftrightarrow\>}
  \newcommand{\contra}{\>\text{\textreferencemark}\>}

  \DeclareMathOperator{\arcosh}{arcosh}
  \DeclareMathOperator{\arsinh}{arsinh}
  \DeclareMathOperator{\artanh}{artanh}
  \DeclareMathOperator{\arsech}{arsech}
  \DeclareMathOperator{\arcsch}{arcsch}
  \DeclareMathOperator{\arcoth}{arcoth}

  \newcommand{\Z}{\mathbb{Z}}
  \newcommand{\Znn}{\mathbb{Z}_{\geq0}}
  \newcommand{\Znz}{\mathbb{Z}_{\neq0}}
  \newcommand{\Zpl}{\mathbb{Z}^+}

  \newcommand{\R}{\mathbb{R}}
  \newcommand{\Rnn}{\mathbb{R}_{\geq0}}
  \newcommand{\Rnz}{\mathbb{R}_{\neq0}}
  \newcommand{\Rpl}{\mathbb{R}^+}
  \newcommand{\CC}{\mathbb{C}}

  \setmainfont[
    Path=./fonts/libertinus/,
    Extension=.otf,
    UprightFont={*-Regular},
    BoldFont={*-Bold},
    ItalicFont={*-Italic},
    BoldItalicFont={*-BoldItalic}
  ]{LibertinusSerif}
  \setmathrm{LibertinusMath-Regular.otf}
  \setsansfont[
    Path=./fonts/libertinus/,
    Extension=.otf,
    UprightFont={*-Regular},
    BoldFont={*-Bold},
    ItalicFont={*-Italic}
  ]{LibertinusSans}
  \setmonofont[
    Path=./fonts/ibm-plex/,
    Extension=.otf,
    UprightFont={*-Regular},
    ItalicFont={*-Italic},
    BoldFont={*-Bold}
  ]{IBMPlexMono}

%%------------------------------------------------------------
%% Environments
%% |- Dependencies
  \RequirePackage{amsthm}
  \RequirePackage[framemethod=tikz]{mdframed}
  \RequirePackage{listings}
  \RequirePackage[shortlabels]{enumitem}
  \ifTS@HyperrefMacros
  \RequirePackage{aliascnt}
  \fi

%% |- Rough theorem styles
%%     No break between heading and body, numbered
  \newtheoremstyle{NoBreak}
    {}% measure of space to leave above the theorem.
    {}% measure of space to leave below the theorem.
    {\itshape}% name of font to use in the body of the theorem
    {}% measure of space to indent
    {}% name of head font
    {}% punctuation between head and body
    {.5em}% space after theorem head
    %; " " = normal inter-word space
    % Manually specify theorem head
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}

%%     No break between heading and body, not numbered
  \newtheoremstyle{NoBreakNoNumber}{}{}{\itshape}% chktex 6
    {}{}{}{.5em}{\thmname{#1}:\thmnote{ #3}}

%%     Break between heading and body, numbered and bold
  \newtheoremstyle{BreakBold}{}{}{\itshape}% chktex 6
    {}{\bfseries}{}{\newline}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}

%%     Break between heading and body, numbered
  \newtheoremstyle{Break}{}{}{\itshape}%
    {}{}{}{\newline}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}

%%     Break between heading and body, not numbered
  \newtheoremstyle{BreakNoNumber}{}{}{\itshape}%
    {}{}{}{\newline}%
    {\thmname{#1}:\thmnote{ #3}}

%%     Break between heading and body, bold heading, not numbered
  \newtheoremstyle{BreakBoldNoNumber}{}{}{\itshape}%
    {}{\bfseries}{}{\newline}%
    {\thmname{#1}:\thmnote{ #3}}

%%     No break between heading and body, bold heading, numbered
  \newtheoremstyle{NoBreakBold}{}{}{\itshape}% chktex 6
    {}{\bfseries}{}{.5em}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}

%%     No break between heading and body, bold heading, not numbered
  \newtheoremstyle{NoBreakBoldNoNumber}{}{}{\itshape}%
    {}{\bfseries}{}{.5em}{\thmname{#1}:\thmnote{ #3}}

%% |- Length definitions
%%    |- Declare lengths
  \newlength{\envVertSkip}
  \newlength{\envTopInner}
  \newlength{\envBotInner}
  \newlength{\envLeftSkip}
  \newlength{\subLeftSkip}
  \newlength{\subTopSkip}
  \newlength{\subBotSkip}
  \newlength{\headerOffset}

%%    |- Define lengths
  \setlength{\envVertSkip}{.5cm}
  \setlength{\envTopInner}{.1cm}
  \setlength{\envBotInner}{.25cm}
  \setlength{\envLeftSkip}{.5cm}
  \setlength{\subLeftSkip}{.1cm}
  \setlength{\subTopSkip}{0cm}
  \setlength{\subBotSkip}{.1cm}
  \setlength{\headerOffset}{-.3cm}

  \renewcommand{\qedsymbol}{$\blacksquare$}

  \newtheoremstyle{Theorem}{}{}{}
    {}{\bfseries}{}{\newline}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}
  \mdfdefinestyle{theorem}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\envTopInner, innerbottommargin=\envBotInner,
    innerleftmargin=\envLeftSkip,
    linecolor=line-blue,
    linewidth=.05cm, backgroundcolor=back-blue!20
    }
  \theoremstyle{Theorem}
  \newmdtheoremenv[style=theorem]{theorem}{Theorem}[\TS@envRefreshLevel]
  \newtheoremstyle{Theorem*}{}{}{}
    {}{\bfseries}{}{\newline}%
    {\thmname{#1}:\thmnote{ #3}}
  \theoremstyle{Theorem*}
  \newmdtheoremenv[style=theorem]{theorem*}{Theorem}

  \newtheoremstyle{Proposition}{}{}{\itshape}%
    {}{\bfseries}{}{\newline}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}
  \newtheoremstyle{Proposition*}{}{}{\itshape}%
  {}{\bfseries}{}{\newline}%
  {\thmname{#1}:\thmnote{ #3}}
  \mdfdefinestyle{proposition}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\envTopInner, innerbottommargin=\envBotInner,
    innerleftmargin=\envLeftSkip,
    backgroundcolor=back-grey!5,
    linecolor=line-grey, theoremtitlefont=\sffamily\bfseries
    }
  \mdfdefinestyle{subPro}
  {
    skipabove=\subTopSkip, skipbelow=\subBotSkip,
    innertopmargin=\headerOffset,
    innerbottommargin=0cm,
    innerleftmargin=\subLeftSkip,
    backgroundcolor=back-grey!5,
    hidealllines=true,
    leftline=true, linecolor=line-grey,
    linewidth=.05cm
  }

  \ifTS@HyperrefMacros
    \theoremstyle{Proposition}
    \newaliascnt{proposition}{theorem}
    \newmdtheoremenv[style=proposition]{proposition}%
    [proposition]{Proposition}
    \aliascntresetthe{proposition}
    \theoremstyle{NoBreakBold}
    % Refreshes every proposition
    \newmdtheoremenv[style=subPro]{subPro}{}[proposition]
    \providecommand*{\propositionautorefname}{Proposition}
    \providecommand*{\subProautorefname}{Proposition}
  \else
    \theoremstyle{Proposition}
    \newmdtheoremenv[style=proposition]{proposition}%
    [theorem]{Proposition}
    \theoremstyle{NoBreakBold}
    % Refreshes every proposition
    \newmdtheoremenv[style=subPro]{subPro}{}[theorem]
  \fi
  \theoremstyle{Proposition*}
  \newmdtheoremenv[style=proposition]{proposition*}%
  {Proposition}

  \newtheoremstyle{Definition}{5pt}{5pt}{\sffamily}%
    {}{\sffamily\bfseries}{}{\newline}%
    {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}
  \newtheoremstyle{Definition*}{5pt}{5pt}{\sffamily}%
    {}{\sffamily\bfseries}{}{\newline}%
    {\thmname{#1}:\thmnote{ #3}}
  \mdfdefinestyle{definition}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\envTopInner, innerbottommargin=\envBotInner,
    innerleftmargin=\envLeftSkip,
    linecolor=line-black,
    linewidth=.05cm,
    nobreak
    }
  \mdfdefinestyle{subDef}
  {
    % skipabove needs to be a bit higher to balance with the lower skip
    skipabove=\dimexpr(\subTopSkip + .25cm), % chktex 1
    skipbelow=\subBotSkip,
    innertopmargin=0cm, %=\dimexpr(\headerOffset + .3cm)
    innerleftmargin=0cm, % we want subDefs to be flush with the main definition
    hidealllines=true,
    topline=true,
    linecolor=line-black,
    linewidth=.01cm
  }
  \ifTS@HyperrefMacros
    \theoremstyle{Definition}
    \newaliascnt{definition}{theorem}
    \newmdtheoremenv[style=definition]{definition}%
    [definition]{Definition}
    \aliascntresetthe{definition}
    \newtheoremstyle{SubDefinition}{}{}{\sffamily}%
      {}{\bfseries}{}{.5em}%
      {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}
    \theoremstyle{SubDefinition}
    % Refreshes every definition
    \newmdtheoremenv[style=subDef]{subDef}{}[definition]
    \providecommand*{\definitionautorefname}{Definition}
    \providecommand*{\subDefautorefname}{Definition}
  \else
    \theoremstyle{Definition}
    \newmdtheoremenv[style=definition]{definition}%
    [theorem]{Definition}
    \newtheoremstyle{SubDefinition}{}{}{\sffamily}%
      {}{\bfseries}{}{.5em}%
      {\thmname{#1}\thmnumber{ #2}:\thmnote{ #3}}
    \theoremstyle{SubDefinition}
    % Refreshes every definition
    \newmdtheoremenv[style=subDef]{subDef}{}[theorem]
  \fi
    \theoremstyle{Definition*}
    \newmdtheoremenv[style=definition]{definition*}%
    {Definition}

  \mdfdefinestyle{example}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\headerOffset, % want the top of the example to be flush with
                                %the bar
    innerbottommargin=0cm, % want the bottom to be flush with the bar
    innerleftmargin=\envLeftSkip,
    hidealllines=true, leftline=true, linecolor=line-black,
    linewidth=.1cm
    }
  \mdfdefinestyle{subExa}
  {
    skipabove=\subTopSkip, skipbelow=\subTopSkip,
    innertopmargin=\headerOffset, % flush with bar
    innerbottommargin=0cm, % ditto
    innerleftmargin=\subLeftSkip,
    hidealllines=true,
    leftline=true,
    linecolor=line-black,
    linewidth=.01cm
  }
  \theoremstyle{BreakBoldNoNumber}
  \newmdtheoremenv[style=example]{example}{Example}
  \theoremstyle{NoBreak}
  \newmdtheoremenv[style=subExa]{subExa}{}
  % Refreshes every example

  \mdfdefinestyle{exercise}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\headerOffset, % want the top of the example to be flush with
                                %the bar
    innerbottommargin=0cm, % want the bottom to be flush with the bar
    innerleftmargin=\envLeftSkip,
    hidealllines=true, leftline=true, rightline=true, linecolor=line-black,
    linewidth=.1cm
    }
  \mdfdefinestyle{subExr}
  {
    skipabove=\subTopSkip, skipbelow=\subTopSkip,
    innertopmargin=\dimexpr(\headerOffset + .2cm),
    innerbottommargin=.25cm, % ditto
    innerleftmargin=\subLeftSkip,
    hidealllines=true,
    topline=true,
    bottomline=true,
    linecolor=line-black,
    linewidth=.01cm
  }
  \theoremstyle{BreakBoldNoNumber}
  \newmdtheoremenv[style=exercise]{exercise}{Exercise}
  \theoremstyle{NoBreak}
  \newmdtheoremenv[style=subExr]{subExr}{}
  % Refreshes every example

  \mdfdefinestyle{lemma}
  {
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\headerOffset, innerbottommargin=0cm, % flush
    innerleftmargin=\envLeftSkip,
    hidealllines=true,
    leftline=true, linecolor=line-grey,
    linewidth=.05cm
  }
  \ifTS@HyperrefMacros
    \theoremstyle{Break}
    \newaliascnt{lemma}{theorem}
    \newmdtheoremenv[style=lemma]{lemma}[lemma]{Lemma}
    \aliascntresetthe{lemma}
    \providecommand*{\lemmaautorefname}{Lemma}
  \else
    \theoremstyle{Break}
    \newmdtheoremenv[style=lemma]{lemma}[theorem]{Lemma}
  \fi
    \theoremstyle{BreakNoNumber}
    \newmdtheoremenv[style=lemma]{lemma*}{Lemma}

  \mdfdefinestyle{corollary}
  {
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\headerOffset, innerbottommargin=0cm, % flush
    innerleftmargin=\envLeftSkip,
    hidealllines=true, leftline=true, linecolor=accent-blue,
    linewidth=.05cm
    }
    \ifTS@HyperrefMacros
      \theoremstyle{BreakBold}
      \newmdtheoremenv[style=corollary]{corollary}%
        {Corollary}[theorem]
      \providecommand*{\corollaryautorefname}{Corollary}
    \else
      \theoremstyle{BreakBold}
      \newmdtheoremenv[style=corollary]{corollary}%
        {Corollary}[theorem]
    \fi
      \theoremstyle{BreakBoldNoNumber}
      \newmdtheoremenv[style=corollary]{corollary*}%
        {Corollary}

  \mdfdefinestyle{remark}{
    skipabove=\envVertSkip, skipbelow=\envVertSkip,
    innertopmargin=\headerOffset, innerbottommargin=0cm, % flush
    innerleftmargin=\envLeftSkip,
    hidealllines=true, leftline=true, linecolor=line-black
    }
  \ifTS@HyperrefMacros
    \theoremstyle{BreakBold}
    \newaliascnt{remark}{theorem}
    \newmdtheoremenv[style=remark]{remark}[remark]{Remark}
    \aliascntresetthe{remark}
    \providecommand*{\remarkautorefname}{Remark}
  \else
    \theoremstyle{BreakBold}
    \newmdtheoremenv[style=remark]{remark}{Remark}
  \fi
    \theoremstyle{BreakBoldNoNumber}
    \newmdtheoremenv[style=remark]{remark*}{Remark}

  \theoremstyle{NoBreakBoldNoNumber}
  \makeatletter
  \@ifclassloaded{beamer}
  {}
  {
    \newtheorem{note}{Note}
  }
  \makeatother

  \lstset{
    basicstyle=\fontsize{8}{10}\ttfamily,
    aboveskip={0.8\baselineskip},
    belowskip={0.8\baselineskip},
    columns=fixed,
    extendedchars=true,
    breaklines=true,
    tabsize=4,
    prebreak=\raisebox{0ex}[0ex][0ex]%
    {\ensuremath{\hookleftarrow}},
    frame=lines,
    showtabs=true,
    showspaces=false,
    showstringspaces=false,
    keywordstyle=\color{line-blue},
    commentstyle=\color{accent-green},
    stringstyle=\color{accent-red},
    numbers=left,
    numberstyle=\small,
    stepnumber=1,
    numbersep=10pt,
    captionpos=t,
    numbers=left,
    stepnumber=1,
    firstnumber=1,
    numberfirstline=true
    }

%%-----------------------------------------------------------
%% Document Design
%% |- Dependencies
\RequirePackage{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{hyperref}
\RequirePackage{xr}
\RequirePackage{zref}

%% |- Link design
\hypersetup%
{
  colorlinks=true,
  linkcolor=blue,
  filecolor=magenta,
  urlcolor=cyan,
}
\urlstyle{same}

%% |- Document sizing
\ifTwosided % chktex 1
%%    |- Two sided document page spread size
\geometry{a4paper,%
  inner=1.5cm,%
  outer=2.3cm,%
  top=2.7cm,%
  bottom=2.5cm,%
  heightrounded,%
  marginparwidth=0cm,%
  marginparsep=0cm,%
}
\else
%%    |- One sided document page spread size
\geometry{a4paper,%
  inner=1.5cm,%
  outer=1.5cm,%
  top=2.7cm,%
  bottom=2.5cm,%
  heightrounded,%
  marginparwidth=0cm,%
  marginparsep=0cm,%
}
\fi

%%-----------------------------------------------------------
%% Headers

%% |- Variables
\makeatletter
\renewcommand{\title}[1]{%
  \gdef\@title{#1}%
  \gdef\thetitle{#1}% New title variable defined
}
\makeatother

%% |- Header style
\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{\thesection:\ #1}}
\renewcommand{\subsectionmark}[1]{}

%%    |- Two sided document header
\ifTwosided % chktex 1
\fancypagestyle{main}{%
  \fancyhf{} % clear all fields
  \fancyhead[RO]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \thepage}}
  \fancyhead[LO]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \thetitle}}
  \fancyhead[RE]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \rightmark}}
  \fancyhead[LE]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \thepage}}
  \renewcommand{\headrulewidth}{1pt}
  %%      |- Draft document (unused)
  % \ifTS@draft
  % \else
  % \fi
  \setlength{\headheight}{0.3cm}
  \setlength{\headsep}{0.7cm}
  \setlength{\voffset}{-0.3cm}
  % \renewcommand{\headruleskip}{0cm}
  % \setlength{\hoffset}{10pt}
  \renewcommand{\headrule}{\hbox to\headwidth{% chktex 1
      \color{ts-8}\leaders\hrule height \headrulewidth\hfill}}
}
%%    |- One sided document header
\else
\fancypagestyle{main}{%
  \fancyhf{} % clear all fields
  \fancyhead[LO,LE]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \thetitle}}
  \fancyhead[RO,RE]{\raisebox{-.6\baselineskip}[0pt][0pt]{%
      \fontsize{8}{10}\selectfont \rightmark}}
  \lfoot{}
  \cfoot{\thepage}
  \rfoot{}
  \renewcommand{\headrulewidth}{1pt}
  %%      |- Draft document (unused)
  % \ifTS@draft
  % \else
  % \fi
  \setlength{\headheight}{0.3cm}
  \setlength{\headsep}{0.7cm}
  \setlength{\voffset}{-0.3cm}
  % \renewcommand{\headruleskip}{0cm}
  % \setlength{\hoffset}{10pt}
  \renewcommand{\headrule}{\hbox to\headwidth{% chktex 1
      \color{ts-8}\leaders\hrule height \headrulewidth\hfill}}
}
\fi

\fancypagestyle{cover}{%
  \fancyhf{} % clear all fields
  \renewcommand{\headrulewidth}{0pt}
}
\pagestyle{main}

\endinput
%%
%% End of file `texstyle-notes.cls'.
