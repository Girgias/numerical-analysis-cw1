# Numerical analysis CW 1

Graphs plotted within the Latex document.

Computed using PHP 7.4.

## Installation
To test locally ``git clone`` the repository and using PHP's package manager composer install all the dependencies
using the ``composer install`` command.

## Running question 2 polynomial generation

Having PHP 7.4 installed execute the following command ``php question2/a.php`` for part A and
``php question2/b.php`` for part B

## Running unit test suite

Use the custom composer command ``composer test``
