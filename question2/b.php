<?php

declare(strict_types=1);

use Girgias\NumericalAnalysis\CW1\DataParser;
use Girgias\NumericalAnalysis\CW1\LeastSquareSolver;
use MathPHP\LinearAlgebra\MatrixFactory;
use MathPHP\LinearAlgebra\Vector;

// Autoloader to import the classes for usage
$ROOT = \dirname(__DIR__);
require_once $ROOT . '/vendor/autoload.php';

/**
 * An equivalent to MatLab and Python unwrap function.
 */
function unwrap(array $thetas): array
{
    $discontinuity = \M_PI;
    $prior = $thetas[0];
    $piAdjustment = 0;
    $adjustedThetas = [];
    foreach ($thetas as $theta) {
        if (($prior - $theta) > $discontinuity) {
            ++$piAdjustment;
        }
        $adjustedThetas[] = $theta + ($piAdjustment * 2 * \M_PI);
        $prior = $theta;
    }

    return $adjustedThetas;
}

// Retrieve data and parse it
$data = \file_get_contents($ROOT . '/data/Data2');
$dataParsed = new DataParser($data);

$nbRows = $dataParsed->getX()->count();
// The utter right column is the constant polynomial 1
$A = MatrixFactory::one($nbRows, 1);

// x and y points as arrays
$x = $dataParsed->getX();
$y = $dataParsed->getY();

// List of Thetas and Rs
$thetas = [];
$rs = [];
for ($i = 0; $i < \count($x); ++$i) {
    //R² = x² + y²
    $rs[] = \sqrt($x[$i] ** 2 + $y[$i] ** 2);
    /**
     * Therefore the principal value of theta is y/x which is provided
     * by the atan($y, $x) function.
     * Convert it to degrees.
     */
    $thetas[] = \rad2deg(\atan2($y[$i], $x[$i]));
}

$thetas = unwrap($thetas);

// As vectors
$thetas = new Vector($thetas);
$rs = new Vector($rs);

// The utter right column is the constant polynomial 1
$A = MatrixFactory::one($nbRows, 1);
// Add a polynomial factor to A and solve the least square problem
$degree = 5;
for ($i = 1; $i <= $degree; ++$i) {
    // Raise each value of the column vector to the power $i
    $columnVector = $thetas->asColumnMatrix()->map(
        static function ($x) use ($i) {
            return $x ** $i;
        }
    );

    // Augment the matrix A and reassign A
    $A = $A->augmentLeft($columnVector);

    // Solve LeastSquareProblem with the given polynomial
    $LSS = new LeastSquareSolver($A, $dataParsed->getY());

    $solution = $LSS->getSolution();
    // Print the solution and its L² norm.
    echo $solution . \PHP_EOL;
    echo 'Norm:' . $solution->l2Norm() . \PHP_EOL;
}
