<?php

declare(strict_types=1);

use Girgias\NumericalAnalysis\CW1\DataParser;
use Girgias\NumericalAnalysis\CW1\LeastSquareSolver;
use MathPHP\LinearAlgebra\MatrixFactory;

// Autoloader to import the classes for usage
$ROOT = \dirname(__DIR__);
require_once $ROOT . '/vendor/autoload.php';

// Retrieve data and parse it
$data = \file_get_contents($ROOT . '/data/Data1');
$dataParsed = new DataParser($data);

$nbRows = $dataParsed->getX()->count();

// The utter right column is the constant polynomial 1
$A = MatrixFactory::one($nbRows, 1);
// Generate A matrix
$degree = 2;
for ($i = 1; $i <= $degree; ++$i) {
    // Raise each value of the column vector to the power $i
    $columnVector = $dataParsed->getX()->asColumnMatrix()->map(
        static function ($x) use ($i) {
            return $x ** $i;
        }
    );

    // Augment the matrix A and reassign A
    $A = $A->augmentLeft($columnVector);
}

$LSS = new LeastSquareSolver($A, $dataParsed->getY());

echo $LSS->getSolution() . \PHP_EOL;
