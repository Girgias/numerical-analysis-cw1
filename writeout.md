# Numerical analysis coursework

## Introduction


The code in this coursework has been written in PHP 7.4 and using
the https://github.com/markrogoyski/math-php MathPHP library which
can be installed using composer.

The source code of this coursework will be available on GitLab at:

## Question 1

The code of the parallel QR decomposition is contained in the PHP class
``ParallelQRDecomposition`` located at ``src/ParallelQRDecomposition.php``

Two of the steps within the parallel algorithm have been split out of the
constructor into their own methods, namely
``getSubMatrices(Matrix $input, int $nbSubMatrices)``which splits the
matrix ``$input`` into ``$nbSubMatrices``number of sub-matrices and
``mergeSubMatrices(Matrix ...$matrices)`` which takes a variable number
of matrices and "merge" them into one by stacking them on top of each
other.

### getSubMatrices()

```php
/**
 * @param Matrix $input         m × n matrix
 * @param int    $nbSubMatrices Number of sub-matrices to partition $input into
 *
 * @throws MatrixException
 *
 * @return array<int, Matrix> Integer indexed list of Matrix objects
 */
public static function getSubMatrices(Matrix $input, int $nbSubMatrices): array
{
    /**
     * @var array<int, Matrix>
     */
    $list = [];

    // As indexes start at 0 we need to shift every offset by -1
    $rows = $input->getM() - 1;
    $columns = $input->getN() - 1;

    // Sanity check, verify that there are sufficient rows in the $input matrix.
    if ($rows < $nbSubMatrices) {
        throw new \InvalidArgumentException("Can't partition input matrix in more sub-matrices than rows");
    }

    /**
     * How many rows each sub-matrix should have, the rows will be evenly spread therefore we devide by
     * the number of sub-matrices we want.
     * We need to round up (using ceil()) because we shifted the number of rows to rows - 1 as indexes start at 0.
     * This is needed for odd number of row: e.g. 9 rows => max offset = 8 => divided by 3
     *  => number of rows per sub-matrix = 2.66 => rounding up: number of rows per sub-matrix = 3
     * Finally we cast to integer as PHP returns a float as it is possible to pass a second argument for the
     *  precision of the rounding.
     * This last step is technically unnecessary but to ensure type safety we still do it.
     */
    $nbRows = (int) \ceil($rows / $nbSubMatrices);

    // Loop until $nbSubMatrices - 1 as last sub matrix may be one row larger if the number of rows is odd
    for ($i = 0; $i < $nbSubMatrices - 1; ++$i) {
        $startRow = $i * $nbRows;
        // -1 in the end due to offsets starting at 0 so need a slight offset shift.
        $endRow = ($i + 1) * $nbRows - 1;

        /**
         * Append new sub-matrix to the list
         * From library documentation:
         * $Mᵢⱼ   = $A->submatrix($mᵢ, $nᵢ, $mⱼ, $nⱼ)
         * Submatrix of A from row mᵢ, column nᵢ to row mⱼ, column nⱼ
         */
        $list[] = $input->submatrix($startRow, 0, $endRow, $columns);
    }

    $lastSubMatrixInitialRow = $nbRows * ($nbSubMatrices - 1);
    $list[] = $input->submatrix($lastSubMatrixInitialRow, 0, $rows, $columns);

    return $list;
}
```

### mergeSubMatrices()

```php
/**
 * @param Matrix ...$matrices (Variable number of matrices)
 */
public static function mergeSubMatrices(Matrix ...$matrices): Matrix
{
    /**
     * $matrices is an array of Matrix objects and we use array_reduce()
     * to apply the callback on each element of the array.
     */
    return \array_reduce(
        $matrices,
        /**
         * The reason for accepting a nullable carry is that on the first
         * iteration, array_reduce() doesn't have a carry Matrix on which
         * to augment it-self, therefore pass the first entry as the carry.
         */
        static function (?Matrix $carry, Matrix $item): Matrix {
            if (\is_null($carry)) {
                return $item;
            }

            return $carry->augmentBelow($item);
        }
    );
}
```
 