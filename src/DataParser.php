<?php

declare(strict_types=1);

namespace Girgias\NumericalAnalysis\CW1;

use MathPHP\LinearAlgebra\Vector;

class DataParser
{
    private const LARGE_DELIMITER = '    ';
    private const DELIMITER = '   ';
    private Vector $x;
    private Vector $y;

    public function __construct(string $fileStream)
    {
        /**
         * @var array<int, float>
         */
        $x = [];
        /**
         * @var array<int, float>
         */
        $y = [];

        // Explode into rows
        $rows = \explode("\n", $fileStream);

        foreach ($rows as $row) {
            // Remove whitespaces around the entries
            $row = \trim($row);
            // If the row only contains whitespaces skip it
            if ($row === '') {
                continue;
            }
            $row = \str_replace(self::LARGE_DELIMITER, self::DELIMITER, $row);
            [$xi, $yi] = \explode(self::DELIMITER, $row);

            $x[] = (float) $xi;
            $y[] = (float) $yi;
        }

        $this->x = new Vector($x);
        $this->y = new Vector($y);
    }

    public function getX(): Vector
    {
        return $this->x;
    }

    public function getY(): Vector
    {
        return $this->y;
    }
}
