<?php

declare(strict_types=1);

namespace Girgias\NumericalAnalysis\CW1;

use MathPHP\LinearAlgebra\Matrix;
use MathPHP\LinearAlgebra\Vector;

final class LeastSquareSolver
{
    private Vector $x;

    private Matrix $Q;
    private Matrix $R;

    public function __construct(Matrix $A, Vector $y)
    {
        // Compute QR of A matrix
        $qrDecomposition = $A->qrDecomposition();
        $Q = $qrDecomposition->Q;
        $R = $qrDecomposition->R;

        /**
         * Remove numerical zeros.
         */
        $epsilon = $R->getError();
        $R = $R->map(static function ($value) use ($epsilon) {
            if (\abs($value) < $epsilon) {
                return 0;
            }

            return $value;
        });

        $Qᵀ = $Q->transpose();

        // Compute Qᵀ * Y
        $QᵀY = $Qᵀ->multiply($y);
        // Get the matrix as a vector
        $b = $QᵀY->asVectors()[0];

        /**
         * Solve upper triangular system Rx = Qᵀy
         * As R is upper triangular, compute the inverse and then solve
         * x = R⁻¹b.
         */
        $this->x = new Vector($R->inverse()->multiply($b)->getColumn(0));
    }

    public function getSolution(): Vector
    {
        return $this->x;
    }
}
